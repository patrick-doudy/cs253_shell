# Makefile for CS253 p2
# Patrick Doudy Summer 2017

GCC=gcc
CFLAGS=-Wall -MMD
PROG=smash
COMPS=smash.c history.c

all: $(COMPS)
	$(GCC) $(CFLAGS) -o $(PROG) $^

smash: $(COMPS)
	$(GCC) $(CFLAGS) -o $@ $^

-include *.d

run:
	./smash

clean:
	/bin/rm -f *.o a.out
	/bin/rm -f *.d
	/bin/rm -f *~
	/bin/rm -f '#smash.c#'
	/bin/rm -f smash
