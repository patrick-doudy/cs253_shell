/**
 * 'Smash', a basic shell
 * @author Patrick Doudy
 * CS253 Summer 2017
 **/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#include "history.h"
#include "smashConstants.h"


       /* Clear Tokens */

void cleanTokens(char ** tokens, int numTokens){

  int i;
  for(i=0;i<numTokens;i++){
    free(tokens[i]);
  }
}

       /* Deallocate Memory and Exit */

void cleanExit(history * h, char ** tokens, int numTokens){

  cleanTokens(tokens, numTokens);
  free(tokens);
  clear_history(h);
  exit(0);
  
}


       /* Examine Tokens and act as necessary */

void evaluateTokens(char ** tokens, int numTokens, history * h){

  if(tokens[0]=='\0') return;
  
  add_history(h,tokens[0]);

  if( strcmp(tokens[0],"exit") == 0){
    cleanExit(h, tokens, numTokens);
    
  } else if (strcmp(tokens[0],"cd") == 0){
    // cd command
    if(numTokens == 2){
      
      int result = chdir( tokens[1] );
      
      if(result < 0)
	printf("error: %s does not exist\n",tokens[1]);
      else{
	char path[MAXPATHLEN];
	getcwd(path,MAXPATHLEN);
	printf("%s\n",path);
      }
      
    }
    
  } else if (strcmp(tokens[0],"history") == 0) {
    print_history(h);
    
  } else {
    // other command, exec out
      pid_t pid  = fork();
      
      if(pid < 0){
	printf("failed to fork");
	
      } else if (pid > 0) {
	// kill child at random
	int r = rand() % 4;
	if(r==0){
	  kill(pid, SIGKILL);
	} else {
	  int status;
	  waitpid(pid,&status,0);
	}
	
      } else if (pid ==0) {
	execvp(tokens[0], tokens);
	cleanExit(h, tokens, numTokens);
	
      }
    }  

  return;

}

       /* Smash Shell Program Entry */

int main (void) {

  srand((unsigned int) time(NULL));
  
  history * h = init_history();
  char input[MAXLINE];
  char ** tokens = (char **) malloc(MAXARGS *sizeof(char *));
  char * currentToken;
  int numTokens;
  
  // prompt setup
  char prompt[MAXUSERLEN+1];
  prompt[0] = '[';
  prompt[1] = '\0';
  char * userval = getenv("USER");
  strcat(prompt,userval);
  strcat(prompt,"@SMASH]$ ");
  
  fprintf(stderr,"%s",prompt);
  
  // read from stdin & tokenize input
  while(fgets(input, MAXLINE, stdin) != NULL){

    input[strlen(input)-1] = '\0';
    currentToken = strtok(input," ");
    
    numTokens = 0;

    while(currentToken != NULL){

      tokens[numTokens] = (char *) malloc( (1+strlen(currentToken)) * sizeof(char));
      strcpy(tokens[numTokens], currentToken);

      currentToken = strtok(NULL," ");
      numTokens++;
    }

    tokens[numTokens] = NULL;

    // evaluate  commands
    evaluateTokens(tokens, numTokens, h);

    cleanTokens(tokens, numTokens);
    numTokens = 0;
    
    // re-promt user
    fprintf(stderr,"%s",prompt);
  }

  // clean up on broken loop
  cleanExit(h, tokens, numTokens);
  
  return 0;
}
