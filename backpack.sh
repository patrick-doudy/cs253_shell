#!/bin/bash

#
# Global Variables
#
exe=smash
project=P4
dest=""

#
# Generic functions for all projects.
#
function checkArgs_setDest()
{
	local __dest="$1"
	if [ $# != 1 ];then
		echo -e "usage: $0 <output file>"
		echo -e "   output file - the file to save the grades in"
		exit -1
	fi
	rm -f $__dest
	dest=$__dest
}

function test_build()
{
	if [ ! -e "Makefile" ];then
		echo "$project: FAIL, no Makefile found..." >> $dest
		exit 1
	fi

	# validate makefile execution
	make
	if [ $? != 0 ];then
		echo "$project: FAIL, make failed..." >> $dest
		exit 1
	fi
	# validate executable created
	if [ ! -e "$exe" ];then
		echo "$project: FAIL, $exe not created..." >> $dest
		exit 1
	fi
}

function test_clean()
{
	# validate clean execution
	make clean
	if [ $? != 0 ];then
		echo "$project: FAIL, make clean failed..." >> $dest
		exit 1
	fi
	# validate executable removed
	if [ -e "$exe" ];then
		echo "$project: FAIL, clean did not remove $exe..." >> $dest
		exit 1
	fi
}

#
# Functions dependent on this project.
#
function projectExit()
{
	local __stdMsg=$1
	local __exitStatus=$2
	local __trash=$3

	echo "$project: FAIL, $__stdMsg" >> $dest
	turnOnOff_kill "remove_comments"
	if [ "$__trash" != "" -a -e $__trash ];then
		rm -f $__trash
	fi
	exit $__exitStatus
}

function sanityCheck()
{
	# bare minimum sanity test
	./$exe <<- EOF 2> /dev/null
	exit
	EOF

	# validate bare minimum program execution
	if [ $? != 0 ];then
		projectExit "exit command failed..." 2
	fi
}

function test_valgrind()
{
	local __file=temp.trash
	local passMsg="ERROR SUMMARY: 0 errors from 0 contexts"

	# execute program in valgrind with wide range of commands
	valgrind --leak-check=full ./$exe 2>$__file 1>/dev/null <<- EOF
	cd $HOME
	history
	ls -l -a
	pwd
	hello world tokens more tokens
	exit
	EOF

	# validate valgrind execution passes
	if [ $? != 0 ];then
		projectExit "valgrind failed execution..." 2
	fi

	# validate no memory leaks found in valgrind
	grep "$__passMsg" $__file 1>/dev/null 2>/dev/null
	if [ $? != 0 ];then
		projectExit "valgrind error(s) from 1 or more contexts..." 2 $__file
	fi
	rm -f $__file
}

#
# Specific functions for this project.
#
function test_findCmds()
{
	local __requiredCmds=(
		"fgets("
		"chdir("
		"getcwd("
		"strtok("
		"fork("
		"execvp("
		"waitpid("
		"kill("
	)
	# validate required functions appear in c file
	local __look=""
	for __cmd in ${__requiredCmds[@]};do
		# may want to remove recursive grep here...
		__look=$(grep -r $__cmd .)
		if [[ "$__look" != *".c"* ]];then
			echo "$project: FAIL, $__cmd) command never invoked..." >> $dest
			# choosing not to exit if errors exists here
		fi
	done
}

function test_builtins()
{
	local __file=temp.trash

	# execute program with required built-in commands
	./$exe 1>$__file 2>/dev/null <<- EOF
	cd $HOME
	history
	exit
	EOF

	# validate builtIns dont fail program execution
	if [ $? != 0 ];then
		projectExit "$exe failed builtin commands..." 3 $__file
	fi

	# validate cd builtIn command
	local __dir=$(head -1 $__file)
	if [ "$__dir" != "$HOME" ];then
		echo -e "$project: FAIL, cd command failed..." >> $dest
		echo -e "stdout read after 'cd $HOME' was:\n$__dir" >> $dest
		# choosing not to exit if errors exists here
	fi
	sed -i '1d' $__file

	# validate history builtIn command
	local __expectedHist=(
		"$(egrep -w '0|cd|home' $__file 1>/dev/null 2>/dev/null && echo $?)"
		"$(egrep -w '1|history' $__file 1>/dev/null 2>/dev/null && echo $?)"
	)
	for __pf in "${__expectedHist[@]}";do
		if [ "$__pf" != "0" ];then
			echo -e "$project: FAIL, history command failed..." >> $dest
			echo -e "stdout read after 'history' was:\n$(cat $__file)" >> $dest
			# choosing not to exit if errors exists here
		fi
	done
	rm -f $__file
}

function test_unixCmds()
{
	local __file=temp.trash

	# execute program with small set of commands NOT built-in
	./$exe 1>$__file 2>/dev/null <<- EOF
	pwd
	ls -a -l
	exit
	EOF

	# validate typical unix commands dont fail program execution
	if [ $? != 0 ];then
		projectExit "$exe failed typical unix commands..." 3 $__file
	fi

	# validate pwd unix command
	grep "$(pwd)" $__file
	if [ $? != 0 ];then
		echo -e "$project: FAIL, pwd command failed..." >> $dest
		# choosing not to exit if errors exists here
	fi

	# validate ls unix command
	grep "\btotal " $__file
	if [ $? != 0 ];then
		echo -e "$project: FAIL, ls command failed..." >> $dest
		# choosing not to exit if errors exists here
	fi
	rm -f $__file
}

function test_processes()
{
	local __log=system-trace.log

	# trace system calls of executable and it's child processes
	strace -f -o $__log ./$exe <<- EOF
	sleep 0.25
	exit
	EOF

	# validate child process was created
	local __childPID=$(grep "clone(" $__log | awk -F " = " '{print $2}' 2>/dev/null)
	if [ "$__childPID" == "" ];then
		projectExit "child process never created 'sleep 0.25'..." 3 $__log
	fi

	# validate exit status & correct system call of child process
	grep "\bexecve(.*/sleep.* = 0" $__log 1>/dev/null 2>/dev/null
	local __success=$?
	if [ "$__success" != 0 ];then
		projectExit "child process did not execute 'sleep 0.25' properly..." 3 $__log
	fi
	rm -f $__log
}

function turnOnOff_kill()
{
        for __cfile in $(grep -r "\bkill(" *.c | awk -F ":" '{print $1}');do
                # comment out kill invocation
                if [ "$1" == "insert_comments" ];then
                        sed -i 's:\bkill(://kill(:g' $__cfile
                # remove comments around kill invocation
                elif [ "$1" == "remove_comments" ];then
                        sed -i 's://kill(:kill(:g' $__cfile
                fi
        done
}

function test_project()
{
	# prevent killing of child processes
	make clean
	turnOnOff_kill "insert_comments"
	make

	sanityCheck
	test_findCmds
	test_builtins
	test_unixCmds
	test_processes
	test_valgrind

	make clean
	turnOnOff_kill "remove_comments"
	if [ -e $dest ];then
		echo "$project: FAIL." >> $dest
		exit 4
	fi
}


#
# Run Backpack Script
#
checkArgs_setDest $@
test_build
test_clean
test_project
echo "$project: PASS" >> $dest
echo -e "\n$project: PASS\n"

