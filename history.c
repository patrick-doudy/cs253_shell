/**
 * Function Bodies for use with
 * smash command history
 * @author Patrick Doudy
 * CS253 Summer 2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "history.h"


     /* create history container */

history * init_history(void){
  history * h = (history *) malloc(sizeof(history));
  h->size = 0;
  h->first = NULL;
  return h;
}


     /* add a new item to the history */

void add_history(history * h, char * command){

  // create and initialize new item
  historyItem * newItem = (historyItem *) malloc(sizeof(historyItem));
  strcpy(newItem->cmd, command);
  newItem->next = NULL;

  // insert at the end of the history list
  if(h->first==NULL){
    h->first = newItem;
  } else {
    historyItem * c = h->first;
    while(c->next != NULL){
      c = c->next;
    }
    c->next = newItem;
  }
  
  h->size += 1;
  return;
}


     /* print out the history */

void print_history(history * h){
  historyItem * currentItem = h->first;
  int i = 0;
  while(currentItem != NULL){
    printf("%d %s\n", i, currentItem->cmd);
    currentItem = currentItem->next;
    i++;
  }
  return;
}


     /* free memory associated with history */

void clear_history(history * h){
  if(h->first == NULL)
    return;
  
  historyItem * currentItem = h->first;
  historyItem * nextItem = currentItem->next;

  while(nextItem != NULL){
    free(currentItem);
    currentItem = nextItem;
    nextItem = nextItem->next;
  }
   
  free(currentItem);
  free(h);

  return;
}
