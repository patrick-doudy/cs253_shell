Feedback comments for -> patrickdoudy,

Grading Scripts:
================
-> passing scripts, good.

Code Quality:
================
-> Good solution for keeping track of parsed tokens from input.
-> Great function for parsing input!
-> Proper use of execvp()... good job here.
-> Valid implementation of random killing of child process... good job here.
-> Good code formatting.
-> Great comments!
-> Great variable names.
-> Good use of functions to modularize code.

README:
================
-> Good README. Follow the same format for future projects.
your text -> "encountering segmentation faults when empty input was supplied"
-> good discription and solution here... good job.

======================
Grade: 75/75
======================
50/50  Grading-Scripts.
-0
15/15  Code-Quality.
-0     code formatting
-0     code comments/doc
-0     code modularity
-0     code semantics
10/10  README.md-File.
-0     used template
-0     grammar/spelling
-0     correct statements
-0     sources

-0     Late-Penalty.
+0     Extra Credit.
======================

Cheers,
Joey.