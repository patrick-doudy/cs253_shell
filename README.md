#Project 4

  * Author: Patrick Doudy
  * Class: CS253 Summer 2017

## Overview

"Smash" is a basic shell program that interacts with Linux system calls. It takes
string input from stdin, tokenizes it and executes system commands defined by
the tokens.

The commands "cd", "history", and "exit" are explicily defined within the shell.

    * "cd" changes the directory of the Smash process
    * "history" prints out previous commands in the session, which are stored
      as a linked list of history items.
    * "exit" will invoked a series of memory deallocations and then terminate
      Smash

When the shell does not recognize a token as one of those commands it forks out
a child process and attempts to execute the program named by the first token,
supplying the subsequent tokens as arguments.

According to the assignment spec, every time a fork is invoked the new child
is killed with a probability of 25%

## Compiling and using

Smash is compiled using the provided Makefile and the straightforward
command "make". Like Bash, Smash can invoke any programs that can be found using
the PATH variable.

## Testing

The shell was tested with a collections of arbitrary inputs, and by following
along the commands named in the supplied test files to ensure the output of
Smash matched what was expected.

I changed the token storage from a statically allocated arrary to a dynamically
allocated array, afterwhich I began encountering segmentation faults when
empty input was supplied. In processing tokens I've been replacing the
accompanying newline character with the null terminator, and when Smash
tried to evaluate the "token" that came from empty input it attempted to
dereference '\0'. Processing the input is simply aborted when this condition
is recognized.

## Sources Used

https://linux.die.net/man/3/execvp
https://stackoverflow.com/questions/822323/how-to-generate-a-random-number-in-c