/**
 * Constants used in Smash
 * @author Patrick Doudy
 * CS253 Summer 2017
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define MAXLINE 64
#define MAXARGS 2048
#define MAXPATHLEN 1024
#define MAXUSERLEN 64

#endif
