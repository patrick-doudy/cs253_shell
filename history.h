/**
 * Definitions for Command History
 * @author Patrick Doudy
 * CS253 Summer 2017
 */

#ifndef HISTORY_H
#define HISTORY_H

#include "smashConstants.h"

typedef struct historyItem_struct historyItem;
typedef struct history_struct history;

struct historyItem_struct {
  char cmd[MAXLINE];
  historyItem * next;
};

struct history_struct {
  int size;
  historyItem * first;
};

history * init_history(void);

void add_history(history *,char *);

void print_history(history *);

void clear_history(history *);

#endif
